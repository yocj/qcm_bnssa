#!/usr/bin/env python3

import requests
import json
import os

URL_INFOS = "https://gitlab.com/yocj/qcm_bnssa/-/raw/master/res/infos.json"
URL_QCM = "https://gitlab.com/yocj/qcm_bnssa/-/raw/master/res/questions.json"

"""
    return values :
        qcm_version : bool, soft_version : bool
"""

def new_updates():
    try:
        infos = requests.get(URL_INFOS)
        qcm_version = infos.json()["qcm_version"]
        soft_version = infos.json()["software_version"]
        current_infos = json.load(open("./res/infos.json", "r", encoding="utf8"))
        current_qcm_version = current_infos["qcm_version"]
        current_soft_version = current_infos["software_version"]

        if current_soft_version != soft_version:
            return False, True
        elif current_qcm_version != qcm_version:
            return True, False
        else:
            return False, False
    except Exception as err:
        return err, err

def update():
    try:
        new_questions = requests.get(URL_QCM).text
        new_infos = requests.get(URL_INFOS).text
        with open("./res/questions.json", "w", encoding="utf8") as f:
            f.write(new_questions)
        with open("./res/infos.json", "w", encoding="utf8") as f:
            f.write(new_infos)
        return 1

    except Exception as err:
        return err