#!/usr/bin/env python3

import tkinter as tk
import tkinter.ttk as ttk
from tkinter import messagebox
from tkinter import filedialog
from tkcalendar import DateEntry
from threading import Thread
import gen_qcm
import updater
import webbrowser

class Application(tk.Frame):
    def __init__(self, master=None):
        master.title("QCM BNSSA")
        master.geometry("500x250")
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()

        self.updates_checker()

    def create_widgets(self):
        self.lbl_title = tk.Label(text="Générateur questionnaire BNSSA", font=("Helvetica", 16))
        self.lbl_title.pack()

        self.btn_quit = ttk.Button(text="Quitter", command=self.master.quit)
        self.btn_quit.pack(side="bottom")

        self.lbl_date = tk.Label(text="Date de l'examen")
        self.lbl_date.pack()

        self.date = DateEntry()
        self.date.pack()

        self.btn_gen = ttk.Button(text="Générer questionnaires", command=self.gen_qcm)
        self.btn_gen.pack()

        self.btn_update = ttk.Button(text="Vérifier les mises à jour", command=self.new_updates)
        self.btn_update.pack()

    def gen_qcm(self):
        path = filedialog.askdirectory()
        t = Thread(target = gen_qcm.select_questions, args = (path, self.date.get_date()))
        t.start()

    # Check updates on update button click
    def new_updates(self):
        res_qcm, res_soft = updater.new_updates()
        if res_soft == True:
            res_q = messagebox.askyesno("", "Nouvelle mise à jour du logiciel disponible, voulez-vous la télécharger ?")
            if res_q:
                webbrowser.open("https://gitlab.com/yocj/qcm_bnssa/-/releases", new=2)
        elif res_qcm == True:
            res_q = messagebox.askyesno("", "Nouvelle mise à jour du qcm disponible, voulez-vous la télécharger ?")
            if res_q:
                c = updater.update()
                if c == 1:
                    messagebox.showinfo("", "Fait")
                else:
                    ShowError(c)
        elif not res_qcm and not res_soft:
            messagebox.showinfo("", "Pas de nouvelle mise à jour")
        else:
            ShowError(res_qcm)

    # check updates on startup
    def updates_checker(self):
        res_qcm, res_soft = updater.new_updates()
        if res_soft == True:
            res_q = messagebox.askyesno("", "Nouvelle mise à jour du logiciel disponible, voulez-vous la télécharger ?")
            if res_q:
                webbrowser.open("https://gitlab.com/yocj/qcm_bnssa/-/releases", new=2)
        elif res_qcm == True:
            res_q = messagebox.askyesno("", "Nouvelle mise à jour du qcm disponible, voulez-vous la télécharger ?")
            if res_q:
                c = updater.update()
                if c == 1:
                    messagebox.showinfo("", "Fait")
                else:
                    ShowError(c)

class ShowError(tk.Tk):
    def __init__(self, details):
        super().__init__()
        self.title("Erreur")
        self.geometry("350x75")
        self.minsize(350, 75)
        self.details_shown = False
        self.resizable(False, False)
        self.rowconfigure(0, weight=0)
        self.rowconfigure(1, weight=1)
        self.columnconfigure(0, weight=1)

        btn_frame = tk.Frame(self)
        btn_frame.grid(row=0, column=0, sticky='nsew')
        btn_frame.columnconfigure(0, weight=1)
        btn_frame.columnconfigure(1, weight=1)

        txt_frame = tk.Frame(self)
        txt_frame.grid(row=1, column=0, padx=(7, 7), pady=(7, 7), sticky='nsew')
        txt_frame.rowconfigure(0, weight=1)
        txt_frame.columnconfigure(0, weight=1)

        ttk.Label(btn_frame, text="Une erreur est survenue").grid(row=0, column=0, columnspan=3, padx=(7, 7), pady=(7, 7), sticky='w')
        ttk.Button(btn_frame, text="Ok", command=self.destroy).grid(row=1, column=1, sticky='e')
        ttk.Button(btn_frame, text="Détails", command=self.show_details).grid(row=1, column=2)

        self.textbox = tk.Text(txt_frame, height=6)
        self.textbox.insert('1.0', details)
        self.textbox.config(state='disabled')
        self.scrollb = tk.Scrollbar(txt_frame, command=self.textbox.yview)
        self.textbox.config(yscrollcommand=self.scrollb.set)

    def show_details(self):
        if self.details_shown:
            self.textbox.grid_forget()
            self.scrollb.grid_forget()
            self.resizable(False, False)
            self.geometry("350x75")
            self.details_shown = False
        else:
            self.textbox.grid(row=0, column=0, sticky='nsew')
            self.scrollb.grid(row=0, column=1, sticky='nsew')
            self.resizable(True, True)
            self.geometry("450x160")
            self.details_shown = True


if __name__ == "__main__":
    root = tk.Tk()
    root.iconphoto(True, tk.PhotoImage(file='./res/icon.png'))
    gui = Application(master=root)
    gui.mainloop()