#!/usr/bin/env python3

from fpdf import FPDF
import os
import json
import shutil
import random


def get_bounds():
    q_max = 40

    q = [0,0,0,0,0,0]

    part_one_min = 6
    part_one_max = 10

    part_two_min = 6
    part_two_max = 10

    part_three_min = 5
    part_three_max = 10

    part_four_min = 5
    part_four_max = 8

    part_five_min = 5
    part_five_max = 8

    part_six_min = 3
    part_six_max = 5

    q[0] = random.randint(part_one_min, part_one_max)
    q[1] = random.randint(part_two_min, part_two_max)
    q[2] = random.randint(part_three_min, part_three_max)
    q[3] = random.randint(part_four_min, part_four_max)
    q[4] = random.randint(part_five_min, part_five_max)
    q[5] = random.randint(part_six_min, part_six_max)

    c = 0
    for i in range(0, len(q)):
        c += q[i]

    if c != q_max:
        return get_bounds()
    else:
        return q

def select_questions(path, date):

    qcm = []
    bounds = get_bounds()
    questions_json = open("./res/questions.json", "r", encoding="utf8")
    questions = json.load(questions_json)["questions"]

    for i in range(0, bounds[0]):
        index = random.randint(0, len(questions["part1"]) - 1)
        qcm.append(questions["part1"][index])
        questions["part1"].pop(index)

    for i in range(0, bounds[1]):
        index = random.randint(0, len(questions["part2"]) - 1)
        qcm.append(questions["part2"][index])
        questions["part2"].pop(index)

    for i in range(0, bounds[2]):
        index = random.randint(0, len(questions["part3"]) - 1)
        qcm.append(questions["part3"][index])
        questions["part3"].pop(index)

    for i in range(0, bounds[3]):
        index = random.randint(0, len(questions["part4"]) - 1)
        qcm.append(questions["part4"][index])
        questions["part4"].pop(index)

    for i in range(0, bounds[4]):
        index = random.randint(0, len(questions["part5"]) - 1)
        qcm.append(questions["part5"][index])
        questions["part5"].pop(index)

    for i in range(0, bounds[5]):
        index = random.randint(0, len(questions["part6"]) - 1)
        qcm.append(questions["part6"][index])
        questions["part6"].pop(index)


    create_pdfs(qcm, path + "/qcm_examen_" + date.strftime("%d_%m_%Y"), date)

def create_pdfs(p_qcm, p_path, date):

    if os.path.exists(p_path):
        shutil.rmtree(p_path)
    os.makedirs(p_path)

    q = FPDF()
    a = FPDF()
    
    q.add_page()
    a.add_page()
    
    """
        FLYLEAF
    """

    q.set_fill_color(239, 243, 247)
    q.rect(0, 3, 210, 40, "F")
    a.set_fill_color(239, 243, 247)
    a.rect(0, 3, 210, 40, "F")

    #logo
    q.image("res/protec.png", 25, 7, 32, 32)
    a.image("res/protec.png", 25, 7, 32, 32)

    q.set_font("Arial", "", 14)
    q.set_text_color(25, 34, 168)
    q.cell(110)
    q.cell(50, 5, "Protection Civile du Bas-Rhin", 0, 1, "L")
    a.set_font("Arial", "", 14)
    a.set_text_color(25, 34, 168)
    a.cell(110)
    a.cell(50, 5, "Protection Civile du Bas-Rhin", 0, 1, "L")

    q.set_font("Arial", "", 12)
    q.set_text_color(67, 120, 181)
    q.cell(118)
    q.cell(50, 5, "Direction Départementale de la formation", 0, 1, "C")
    a.set_font("Arial", "", 12)
    a.set_text_color(67, 120, 181)
    a.cell(118)
    a.cell(50, 5, "Direction Départementale de la formation", 0, 1, "C")

    q.set_font("Arial", "", 10)
    q.cell(130)
    q.cell(50, 5, "15, rue Schertz - 67100 Strasbourg", 0, 1, "R")
    q.cell(130)
    q.cell(50, 5, "03 88 83 75 24", 0, 1, "R")
    q.cell(130)
    q.cell(50, 5, "formation@protectioncivile67.fr -- protectioncivile67.fr", 0, 1, "R")
    q.cell(0, 5, "", 0, 1)
    a.set_font("Arial", "", 10)
    a.cell(130)
    a.cell(50, 5, "15, rue Schertz - 67100 Strasbourg", 0, 1, "R")
    a.cell(130)
    a.cell(50, 5, "03 88 83 75 24", 0, 1, "R")
    a.cell(130)
    a.cell(50, 5, "formation@protectioncivile67.fr -- protectioncivile67.fr", 0, 1, "R")
    a.cell(0, 5, "", 0, 1)

    q.set_font("Arial", "B", 22)
    q.set_text_color(26, 15, 144)
    q.cell(0, 25, "Brevet National de Sécurité et de", 0, 1, "C")
    q.cell(0, 0, "Sauvetage Aquatique", 0, 1, "C")
    a.set_font("Arial", "B", 22)
    a.set_text_color(26, 15, 144)
    a.cell(0, 25, "Brevet National de Sécurité et de", 0, 1, "C")
    a.cell(0, 0, "Sauvetage Aquatique", 0, 1, "C")

    q.set_font("Arial", "B", 20)
    q.set_text_color(0, 0, 0)
    q.cell(0, 175, "Épreuve n°4 du " + date.strftime("%d/%m/%Y"), 0, 1, "C")
    q.cell(0, -150, "- Questionnaire à choix multiple -", 0, 1, "C")
    a.set_font("Arial", "B", 20)
    a.set_text_color(0, 0, 0)
    a.cell(0, 175, "Épreuve n°4 du " + date.strftime("%d/%m/%Y"), 0, 1, "C")
    a.cell(0, -150, "- Correction du QCM -", 0, 1, "C")

    q.set_font("Arial", "", 8)
    q.rect(175, 250, 25, 5)
    q.rect(175, 255, 25, 10)
    q.text(180, 254, "N° candidat")

    q.set_draw_color(248, 208, 181)
    q.line(0, 280, 210, 280)
    q.set_draw_color(0, 0, 0)
    a.set_draw_color(248, 208, 181)
    a.line(0, 280, 210, 280)
    a.set_draw_color(0, 0, 0)

    q.set_font("Arial", "B", 6)
    q.set_text_color(10, 80, 160)
    q.text(19, 288, "MEMBRE DE LA FÉDÉRATION NATIONALE DE PROTECTION CIVILE - RECONNUE D'UTILITÉ PUBLIQUE - DÉCRET DU 14 NOVEMBRE 1969 ET ARRÊTÉ DU 15 OCTOBRE")
    q.text(16, 291, "1996 - AGRÉÉE DE SÉCURITÉ CIVILE PAR LE MINISTÈRE DE L'INTÉRIEUR LE 30 AOÛT 2006 - CONVENTION NATIONALE AVEC LE MINISTRE DE LA SANTÉ - 10 JANVIER 1992")
    a.set_font("Arial", "B", 6)
    a.set_text_color(10, 80, 160)
    a.text(19, 288, "MEMBRE DE LA FÉDÉRATION NATIONALE DE PROTECTION CIVILE - RECONNUE D'UTILITÉ PUBLIQUE - DÉCRET DU 14 NOVEMBRE 1969 ET ARRÊTÉ DU 15 OCTOBRE")
    a.text(16, 291, "1996 - AGRÉÉE DE SÉCURITÉ CIVILE PAR LE MINISTÈRE DE L'INTÉRIEUR LE 30 AOÛT 2006 - CONVENTION NATIONALE AVEC LE MINISTRE DE LA SANTÉ - 10 JANVIER 1992")

    q.add_page()
    q.add_page()
    q.set_font("Arial", "", 10)
    q.set_text_color(0, 0, 0)
    a.set_font("Arial", "", 10)
    a.set_text_color(0, 0, 0)
    a.add_page()
    a.add_page()

    for i in range(0, len(p_qcm)):
        if i % 6 == 0 and i != 0:
            q.add_page()
            a.add_page()
        elif i == 0:
            q.set_font("Arial", "B", 12)
            q.cell(0, 20, "Cochez la ou les lettre(s) correspondant à la ou aux bonne(s) réponse(s) pour chaque question.", 0, 1, "C")
            q.set_font("Arial", "", 10)

        q.multi_cell(0, 5, str(i + 1) + ". " + p_qcm[i]["name"], 0, 1)
        a.multi_cell(0, 5, str(i + 1) + ". " + p_qcm[i]["name"], 0, 1)
        q.ln(3)
        a.ln(3)

        for j in range(0, len(p_qcm[i]["answers"])):
            
            if p_qcm[i]["answers"][j]["isCorrect"] == "true":
                a.set_font("Arial", "B", 10)
                a.cell(5, 5, p_qcm[i]["answers"][j]["number"], 0)
                a.set_font("Arial", "", 10)
                a.multi_cell(0, 5, p_qcm[i]["answers"][j]["text"], 0, 1)
                a.ln(3)

            q.cell(5, 5, "", 1)
            q.set_font("Arial", "B", 10)
            q.cell(5, 5, p_qcm[i]["answers"][j]["number"], 0)
            q.set_font("Arial", "", 10)
            q.multi_cell(0, 5, p_qcm[i]["answers"][j]["text"], 0, 1)
            q.ln(3)
        q.ln(5)

    q.add_page()
    q.output(p_path + "/qcm.pdf")
    a.output(p_path + "/qcm_correction.pdf")
